'''
Created on Feb 17, 2014

This script reads multiple instances of data of the same model and create multiple copies of the model XML for different instance of data
The Models in the Cyphy must obey the Naming convention that begins with Capital Letter and nested component should not have the same name with Models

@author: dixiao
'''

import argparse
import os
import shutil
import distutils.archive_util
import zipfile

if __name__ == '__main__':
    
    CAD_flag = False 
 
    parser = argparse.ArgumentParser(description='Zip Files')
    parser.add_argument("targetDir", help="the folder that cantains instances folder")
    parser.add_argument("-CAD","--CADDir", help="the folder contains CAD files")
    
    args = parser.parse_args()
    print "Attention: Empty folders will be ignored during making archive process"
    if args.CADDir:
        print "CAD is specified"
        CAD_flag = True
    else:
        print "No CAD is specified"
        CAD_flag = False 
    
    targetDir = args.targetDir
    CADDir = args.CADDir

    print targetDir, CADDir
    
    for item in os.listdir(targetDir):
        if os.path.isdir(targetDir+'\\'+str(item)) and item <> 'CAD':
            if CAD_flag:
                #print os.path.isdir(CADDir)
                if os.path.exists(targetDir+'\\'+str(item)+'\\CAD'):
                    shutil.rmtree(targetDir+'\\'+str(item)+'\\CAD')
                shutil.copytree(CADDir, targetDir+'\\'+str(item)+'\\CAD')
                            
            shutil.make_archive(targetDir+'\\'+str(item),format='zip', root_dir=targetDir+'\\'+str(item))