'''
Created on Feb 17, 2014

This script reads multiple instances of data of the same model and create multiple copies of the XML for different instance of data
The Models in the Cyphy must obey the Naming convention that begins with Capital Letter and nested component should not have the same name with Models

@author: dixiao
'''

import xml.etree.cElementTree as ET
import argparse
import sys
import string

if __name__ == '__main__':
    
    componentName = ""
    PropertyList = list()
    ValuesList = list()
    
    parser = argparse.ArgumentParser(description='Write data into Cyphy')
    parser.add_argument("data_file", help="the filename(no space) of data file,")
    parser.add_argument("cyphy_file", help="the filename(no space) of Cyphy XML file")
    args = parser.parse_args()
    
    datafilename = args.data_file
    CyphyFilename = args.cyphy_file

    print args.data_file, args.cyphy_file
    
    data = open(args.data_file)
    
    
    models = list()
    
    
    if not data:
        sys.exit("read data file error")
    else:
        lines = data.readlines()
        for line in lines:
            if line[0] == '{':
                componentName = line[1:-2] 
                print componentName
            elif line[0] == '[':
                PropertyList = line[1:-2].split(', ')
                print PropertyList
            else:
                temp = line[:-1].split(',')
                ValuesList.append(temp)
                
    #print ValuesList  
    data.close()
    
    tree = ET.ElementTree(file=CyphyFilename)
    
    models = tree.findall(".//model[@kind='Component']/[name='"+componentName+"']")
    numOfModels =  len(models)
    numOfValueSets =  len(ValuesList)
    name = models[0].find("name")
    print models[0].tag, models[0].attrib, name.text
    if numOfModels < 1 or numOfValueSets < 1:
        print "Please check the number of the model instances and the data set. One of them or both is less than 1. Exit"
        sys.exit()
    elif numOfModels < numOfValueSets:
        print "The number of data sets and models does not match. Find "+ str(numOfModels) +" Model instances of "+ componentName + " and "+ str(numOfValueSets) + " Value sets for "+componentName+"."
        print "Could not continue because the number of models instance is less than the number of value sets."
        sys.exit()
    elif numOfModels > numOfValueSets:
        print "The number of data sets and models does not match. Find "+ str(numOfModels) +" Model instances of "+ componentName + " and "+ str(numOfValueSets) + " Value sets for "+componentName+"."
        print "Continue to execute!"    
    
    for i, values in enumerate(ValuesList):
        print i,values
        newName = ""
        for j, property in  enumerate(PropertyList):
            if j < 1:
                newName = newName + property+str(values[j]).replace(".", "")
            property_value = models[i].findall("reference[@kind='Property']/[name='"+property+"']/attribute[@kind='Value']/value")
            if len(property_value) > 1:
                print "Find more than 1 instance of Property"+ property + ", Could not apply the data, skip" 
            elif len(property_value) < 1:
                print "Can't find Property: "+ property + " in this document, skip"
            else:
                print "Modify the property: "+property+" in the model: "+componentName +" from " +property_value[0].text+" to "+ values[j]+ ";"
                property_value[0].text = values[j]    
        name = models[i].find("name")
        name.text =  componentName+"_"+newName
            
                
                    
    f = open(CyphyFilename, 'w') 
    f.write('''<?xml version="1.0" encoding="UTF-8"?>\n''')
    f.write('''<!DOCTYPE project SYSTEM "mga.dtd">\n''')
    content = ET.tostring(tree.getroot(),encoding='utf8', method='xml')
    index = string.find(content, '\n')
    content = content[index+1:]
    f.write(content)
    f.close             

   
                    
        
    
