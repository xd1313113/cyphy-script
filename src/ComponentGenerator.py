'''
Created on Feb 17, 2014

This script reads multiple instances of data of the same model and create multiple copies of the model XML for different instance of data
The Models in the Cyphy must obey the Naming convention that begins with Capital Letter and nested component should not have the same name with Models

@author: dixiao
'''

import xml.etree.cElementTree as ET
import argparse
import sys
import shutil
import re
import os
from zipfile import ZipFile

def checkname(name):
    if re.search('[^\.\s\(\)a-zA-Z0-9_-]', name):
        return False
    else:
        return True
    
if __name__ == '__main__':
    
    componentName = ""
    PropertyList = list()
    ValuesList = list()
    
    parser = argparse.ArgumentParser(description='Write data into Cyphy')
    parser.add_argument("data_file", help="the filename(no space) of data file,")
    parser.add_argument("component_file", help="the filename(no space) of Component XML file")
    args = parser.parse_args()
    
    datafilename = args.data_file
    componentFilename = args.component_file

    print args.data_file, args.component_file
    
    data = open(args.data_file)
    
    abspath =  os.path.abspath(datafilename)
    
    basepath = abspath[:abspath.rfind('\\')]
    os.chdir(basepath)
    print os.getcwd()
    
    
    models = list()
    
    if not data:
        sys.exit("read data file error")
    else:
        line1 = data.readline()
        componentName = re.split(', |,',line1)[0]       
        if componentName == "" or componentName == "":
            print "Illegal Component Name!"
        #print componentName
        line2 = data.readline()
        PropertyList = re.split(', |,',line2[:-1])
        #print PropertyList
        
        lines = data.readlines()      

        #print lines
        if len(lines) < 1:
            print "The file does not contain enough data or in bad format!"
            sys.exit()
        
        for line in lines:
            if line[-1] == '\n':
                line = line[:-1]  
            temp = re.split(', |,',line)
            ValuesList.append(temp)
    
    log = open("LogFor"+componentName+str(hash(componentName))+".log", 'w')
    log.write(componentName+"\n")  
    log.write(str(PropertyList)+"\n")
    print  ValuesList
    #log.write(str(ValuesList)+"\n")      

    data.close()
    
    Namelist = list()
    
    if ValuesList is not None:
        if len(PropertyList) <> len(ValuesList[0]):
            log.write( "The number of values is not equal to the number of the properties"+"\n")
            print "The number of values is not equal to the number of the properties, exit"
            sys.exit()
    
    
    namespaces = {'':'avm','xsd':'http://www.w3.org/2001/XMLSchema'}
    ET.register_namespace('', namespaces[''])
    
    tree = ET.ElementTree(file=componentFilename)
    root = tree.getroot()
    #print root.tag, root.attrib
    modelname = root.get("Name")
    if modelname <> componentName:
        log.write("The name in the data is different from the name in the XML"+"\n")
        print "The name in the data is different from the name in the XML, exit"
        sys.exit()
       
    for i, values in enumerate(ValuesList):
        #print i,values
        nameindex = 1
        newfilename =  'component.acm'
        values[0] = values[0].strip()
        
        if checkname(values[0]) == False:
                log.write("The name "+values[0]+" Contains invalid characters in record: "+str(i)+"\n")
                print "The name "+values[0]+" Contains invalid characters in record: "+str(i)+", exit"
                sys.exit() 
        if values[0] in Namelist:
                log.write("The name "+values[0]+" has duplicates in the document, plese check!\n")
                print "The name "+values[0]+" has duplicates in the document, plese check!, exit"
                sys.exit()
                
        Namelist.append(values[0])
        newfilenamezip = values[0]+'.zip'
        shutil.copy(componentFilename, newfilename)
        log.write(newfilename+"\n")
        tree2 = ET.ElementTree(file=newfilename)

        for j, property in  enumerate(PropertyList):
            if j > 0: # skip the name property.02
                
                property_value = tree2.findall(".//Property[@Name='"+property+"']/Value/ValueExpression/Value")
                
                if len(property_value) > 1:
                    log.write("Find more than 1 instance of Property"+ property + ", Could not apply the data, exit"+"\n")
                    print "Find more than 1 instance of Property"+ property + ", Could not apply the data, exit"
                    sys.exit() 
                elif len(property_value) < 1:
                    log.write("Can't find Property: "+ property + " in this document, exit"+"\n")
                    print "Can't find Property: "+ property + " in this document, exit"
                    sys.exit()
                else:
                    #print property_value[0].tag, property_value[0].text
                    log.write("Modify the property: "+property+" in the model: "+componentName +" from " +str(property_value[0].text)+" to "+ values[j]+ ";"+"\n")
                    property_value[0].text = values[j]
        
        #post processing
        
        elements = tree2.iter()

        for element in elements:
            #print element.tag           
            if element.tag == '{avm}Component':
                element.set('xmlns:xsd','http://www.w3.org/2001/XMLSchema')
                element.set('Name', values[0])
            elif element.tag == 'DomainModel':
                element.set('xmlns','')
                element.set('xmlns:q'+str(nameindex), 'modelica')
                nameindex += 1
            elif element.tag == 'Parameter':
                value = element.find('.//ValueExpression')
                if value is not None:
                    value.set('xmlns:q'+str(nameindex), 'avm')
                    nameindex += 1
            elif element.tag == 'Property':
                element.set('xmlns','')
                element.set('xmlns:q'+str(nameindex), 'avm')
                nameindex += 1
            elif element.tag == 'Connector':
                element.set('xmlns','')
                role = element.find('*')
                if role is not None:
                    role.set('xmlns:q'+str(nameindex), 'modelica')
                    nameindex += 1
            elif element.tag == 'Classifications': 
                element.set('xmlns','')
        
        path = componentName+'\\'+values[0]
        tree2.write(newfilename, encoding='utf-8', xml_declaration=True)
        #create path
        if not os.path.exists(path):os.makedirs(path)
        
        if os.path.exists(path+'\\'+newfilename):
            os.remove(path+'\\'+newfilename)
        shutil.move(newfilename,path+'\\'+newfilename)  
              
        with ZipFile(componentName+'\\'+newfilenamezip, 'w') as myzip:
            myzip.write(path+'\\'+newfilename, arcname = newfilename)
            myzip.close()

        
